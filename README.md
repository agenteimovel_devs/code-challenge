# Code Challenge - Agente Imóvel

## Background

Welcome to the Agente Imóvel code challenge! We decided that using a coding challenge related to our area of business and using our data domain would be an excellent opportunity to introduce 
you to the type of data we are working with and what we may do with that data. We also believe the coding challenge format will provide an excellent opportunity for
you to show off your creativity while using the technologies we use.  

The challenge covers the creation of a page for listing (anúncio) data input that allows the registration of a listing in the system. The second page is a page that lists and
displays the added content. A suggestion can be to think about this page as a search result page. More details on this in the user stories below. For inspiration about the
type of data to be saved for each listing, Google is your friend.

To give you sufficient time to complete this task outside of your current work and other chores we are giving you seven (7) full days, which will include a full weekend. 
In your conversation with our HR representative, make sure you can set a side sufficient time to complete the challenge within these 7 days. We estimate it should take 
no more than 8-10 hrs to get it done.

## Mandatory Work

Fork this repository and then use the provided [Laravel](https://laravel.com/docs/5.6) skeleton to create a web-based application that:

1. Provides a solution to user story 1 and 2 as described below
2. Create a README containing
    1. The technologies you chose to use, what they are used for and why you made your particular choice (be brief but also informative and provide relevant arguments)
    2. Any architecture concepts or design patterns that you decided to implement
    3. Any additional information you feel is relevant to clearly communicate your chosen solution

Provide access to your fork and when you are done, send an email with this information to [flavia@agenteimovel.com.br](mailto:flavia@agenteimovel.com.br).  
Feel free to send an email to [dev@agenteimovel.com.br](mailto:dev@agenteimovel.com.br) if you have any questions, if anything is unclear, confusing or just missing.

## User Stories

##### User Story 1 (Add listing UI)

    As a user, I want to be able to enter new real estate listings and have them saved.
    As a user, in the application UI, I can navigate to an add listing page and create a new listing, specifying the following for each listing:

* The name of the listing
* The number of bedrooms of the listing
* The number of parking spaces of the listing
* The price of the listing
* The full address of the listing
* The description of the listing
* The images of the listing (maximum of 6)

When I click "Save", the listing is saved to the database.

##### User Story 2

    As a user, I want to be able to see a list of my submitted listings.
    As a user, in the application UI, I can navigate to a listings page and see all the listings I have already submitted. On this list, I can see:

* The name of the listing
* The price of the listing
* The address of the listing
* The description of the listing
* An image viewer/slider containing the images of the listing

## Requirements

* The controllers and actions should follow the REST standard
* The listings should be stored in a database
* The front-end must be built with [React.js](https://reactjs.org/)
    * You are free to choose how your components will comunicate with the back-end
    * You can use [Laravel Mix](https://laravel.com/docs/5.6/mix#react) or do it your own way
    * You are also free to use any framework of your preference, such as [Bootstrap](https://getbootstrap.com/), [Foundation](https://foundation.zurb.com/) or any other
* The final application should be deployed to [Heroku](https://devcenter.heroku.com/articles/getting-started-with-php#introduction) or [AWS](https://aws.amazon.com/pt/free/) (both have free tiers)

## Advice

* Try to design and implement your solution as you would do for real production code.
* Show us how you create clean and maintainable code. Build something that we would be happy to contribute to.
* Documentation and maintainability is a must.
* Feel free to add more features and show off!

## Questions

**What database should I use?**  
This is entirely up to you. MySQL, Postgres or any other database of you preference.

**What storage should I use?**  
This is entirely up to you. You can save your images on AWS S3, on disk or somewhere else.

**Should I process the uploaded images in some way?**  
This is not required, but if you feel your application will look better, perform better or work better if you do, feel
free to include image processing.

**What will you be grading me on?**  
Elegance, robustness, simplicity,  security, and understanding of the technologies you use

**Will I have a chance to explain my choices?**  
Feel free to comment your code. If we proceed to a phone interview, we'll be asking questions about why you made the choices you made.
